// Inserting Documents
db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
]);

// Inserting embedded array
db.users.insertOne({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});

// Users with letter 's' in the first name or 'd' in their lastname

db.users.find(
	{$or: [
		{firstName: 
			{$regex: 's', $options: 'i'}
		},
		{lastName: 
			{$regex: 'd', $options: 'i'}
		}
	]
	},
	{
		_id:0,
		firstName: 1,
		lastName: 1	
	}
);

// Users who are from the HR department and age is >= 70
db.users.find(
	{$and: 
	[{department: "HR"},{age: {$gte: 70}}]
}
);

// Users with the letter e in their first name and has an age of <= 30
db.users.find(
	{$and: 
		[{firstName: {$regex: 'e'}},{age: {$lte : 30}}]
	}
)
